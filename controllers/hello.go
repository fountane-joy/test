package controllers

import (
	"net/http"

	services "gitlab.com/fountane-joy/test/services"
)

func Hello(w http.ResponseWriter, _ *http.Request) {
	helloService := services.NewHelloService()
	result := helloService.FakeComputation(5)
	_, err := w.Write([]byte(result))
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
