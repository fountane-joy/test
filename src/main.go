package main

import (
	"log"
	"net/http"
	"time"

	"gitlab.com/fountane-joy/test/controllers"
)

func main() {
	http.HandleFunc("/hello", controllers.Hello)
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		log.Println(r.RequestURI)
		_, err := w.Write([]byte("Hello, World"))
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	})
	log.Println("Starting server and listening on port 3001")

	server := &http.Server{
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 10 * time.Second,
	}
	err := server.ListenAndServe()
	if err != nil {
		panic(err)
	}
}
