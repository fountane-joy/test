package services

import (
	"crypto/rand"
	"math/big"
)

type HelloService struct{}

func MockFunction() string {
	return "Hello World"
}

func NewHelloService() *HelloService {
	return &HelloService{}
}

func (s *HelloService) FakeComputation(number int) string {
	max := big.NewInt(10000)

	// Generate a random big.Int
	// The first argument is a reader that returns random numbers
	// The second argument is the maximum value (not inclusive)
	randInt, _ := rand.Int(rand.Reader, max)
	if number == int(randInt.Int64()) {
		return "Hurrah! you got the number right"
	} else if number > int(randInt.Int64()) {
		return "Too high"
	}
	return "Too low"
}
